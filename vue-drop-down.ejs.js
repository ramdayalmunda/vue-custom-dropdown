const { ref, onBeforeMount, onBeforeUnmount, onMounted, watch } = Vue;
export default {
    template: `

<div class="custom-vue-dropdown-container" @click="toggleDropdown" ref="selectRef" tabindex="-1">
    <div>
        {{
            (value==null && settings?.allowNull)?'null':
            (value=='' && settings?.allowBlankString)?'':
            (value===0 && settings?.allowZero)?0:
            (value===false && settings?.allowFalse)?'false':
            (!value && isNaN(value) && settings?.allowNaN)?'NaN':
            value?value:
            (settings?.unsetText?settings?.unsetText:'Select')
        }}
    </div>
    <ul ref="dropdownRef" class="custom-vue-dropdown fade" :class="showDropDown?'show':''" @scroll="scrollHandler">
        <template v-for="(option, o) in selectionArray">
            <li @click="select($event, o)" :id="'vue-custom-dropdown-option-'+o">
                {{
                    (option==null)?null:
                    (settings?.label? option[settings.label] :option)
                }}
            </li>
        </template>
    </ul>
</div>
    
    `,
    props: {
        "value": ref(), // use this if your variable is of a ref
        "selectionArray": ref(),
        "settings": ref({
            label: null, // if your array is an array of object. use this to choose a field
            value: null, // if the thing you want to bind is a particular field install of the object
            unsetText: "Select", //this is the text that will be displayed when v-model has some negative value; as in null, undefined, blank string, etc
            allowZero: false, // If the v-model is the number 0
            allowNull: false, // If the v-model is a null value
            allowNaN: false,  // If the v-model is NaN
            allowFalse: false,  // If the v-model is Boolean false
            allowBlankString: false,  // If the v-model is a blank String ""
            styleField: false, // if you want to change the style of each item based// true; for string array // it your option is of type object; use this as the field where the fontFamily string is stored on that object
        }),
        "styleSheetLink": ref(),
    },
    setup(props, { emit }) {
        var dropdownRef = ref(null)
        var selectRef = ref(null)
        var showDropDown = ref(false)

        function select(e, optionsIndex) {
            e.stopPropagation()
            closeDropdown()
            if (props?.settings?.value) {
                let value = props["selectionArray"][optionsIndex][props?.settings?.value]
                if (props.vObj) {
                    props.vObj[props.vField] = value
                }
                props.value = value
            } else {
                props.value = props["selectionArray"][optionsIndex]
            }
            emit('change', props.value)

        }
        watch(props.selectionArray, (newArr) => {
            setStyles()
        })

        function setStyles() {
            props.selectionArray.forEach((option, o) => {
                let elem = document.getElementById('vue-custom-dropdown-option-' + o)
                let styleObj = option[props?.settings?.styleField]
                if (!elem || !styleObj) return
                let keys = Object.keys(styleObj)
                if (!keys) return
                keys.forEach(key => {
                    elem.style[key] = styleObj[key]
                })
            })
        }

        function toggleDropdown(e) {
            e?.stopPropagation()
            showDropDown.value = !showDropDown.value
            configureDropdown()
        }

        function configureDropdown() {
            if (showDropDown.value && dropdownRef.value) {
                let bufferPx = 10;
                let ddRect = selectRef.value.getBoundingClientRect()


                let maxHeight = document.body.clientHeight - ddRect.y
                if (maxHeight < document.body.clientHeight / 2) {
                    maxHeight = ddRect.y - ddRect.height - bufferPx
                    dropdownRef.value.style.top = `${bufferPx + ddRect.height}px`
                } else {
                    maxHeight = maxHeight - ddRect.height - bufferPx
                    dropdownRef.value.style.top = `${ddRect.y + ddRect.height}px`
                }
                dropdownRef.value.style.left = `${ddRect.x}px`
                dropdownRef.value.style.maxHeight = `${maxHeight}px`

                dropdownRef.value.style.width = `${ddRect.width}px`
                document.body.append(dropdownRef.value)

            }
        }

        function closeDropdown() {
            showDropDown.value = false
        }

        function scrollHandler(e) {
            let elem = e.target
            let maxDepth = 4
            while (elem) {
                if (!maxDepth--) {
                    elem = null
                    break;
                }
                if (elem == dropdownRef.value) break;
                elem = elem?.parentNode
            }
            if (!elem) closeDropdown()

        }

        onBeforeMount(function () {
            let href = props.styleSheetLink ? props.styleSheetLink : "./custom-dropdown.css"
            if (!document.querySelector(`link[href="${href}"]`)) {
                // Dynamically create a link element
                const cssLink = document.createElement('link');

                // Set the attributes for the link element
                cssLink.rel = 'stylesheet';
                cssLink.type = 'text/css';
                cssLink.href = href;

                // Append the link element to the head of the document
                document.head.appendChild(cssLink);
            }
            document.addEventListener('click', closeDropdown)
            document.addEventListener('wheel', scrollHandler)
        })
        onBeforeUnmount(function () {
            document.removeEventListener('click', closeDropdown)
            document.removeEventListener('wheel', scrollHandler)
        })

        onMounted(function () {
            setStyles()
        })

        return {
            selectRef,
            dropdownRef,
            showDropDown,
            toggleDropdown,
            select,
            scrollHandler,
        }

    }
}