# vue-custom-dropdown

## How to use?

use the vue-drop-down.js if you want to add it on your html file as a script tag
```
<script src="https://gitlab.com/ramdayalmunda/vue-custom-dropdown/-/raw/main/vue-drop-down.js?inline=false">
```

You want to dynamically import this library? Don't worry.
You can use the vue-drop-down.ejs.js file to dynamically import
```
import vueDropDrown from "https://gitlab.com/ramdayalmunda/vue-custom-dropdown/-/raw/main/vue-drop-down.js?inline=false"
```



# Why need this dropdown.

- You might want to customize your dropdowns right. But the default <select> tags does not allow styling. Well then this is the dropdown for you.

[gitlab.com](https://gitlab.com/ramdayalmunda/vue-custom-dropdown) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.
 allowing your project to keep going. You can also make an explicit request for maintainers.
